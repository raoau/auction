﻿CREATE TABLE [dbo].[User] (
    [Id]    UNIQUEIDENTIFIER NOT NULL,
    [Name]  NVARCHAR (50)    NOT NULL,
    [Email] NVARCHAR (100)   NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO

CREATE TABLE [dbo].[Product] (
    [Id]   UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR (100)   NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO

CREATE TABLE [dbo].[Auction] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (100)   NOT NULL,
    [BeginTime] DATETIME         NOT NULL,
    [EndTime]   DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO

CREATE TABLE [dbo].[AuctionProduct] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [AuctionId]    UNIQUEIDENTIFIER NOT NULL,
    [ProductId]    UNIQUEIDENTIFIER NOT NULL,
    [InitialPrice] DECIMAL (16, 2)     NOT NULL,
    [SoldPrice]    DECIMAL (16, 2)     NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AuctionProduct_Auction] FOREIGN KEY ([AuctionId]) REFERENCES [dbo].[Auction] ([Id]),
    CONSTRAINT [FK_AuctionProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id])
);

GO

CREATE TABLE [dbo].[Bid] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [AuctionProductId] UNIQUEIDENTIFIER NOT NULL,
    [UserId]           UNIQUEIDENTIFIER NOT NULL,
    [BidPrice]         DECIMAL (16, 2)     NOT NULL,
	[BidTime]		   DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Bid_AuctionProduct] FOREIGN KEY ([AuctionProductId]) REFERENCES [dbo].[AuctionProduct] ([Id]),
    CONSTRAINT [FK_Bid_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

GO

