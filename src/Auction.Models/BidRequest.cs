﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Auction.Models
{
    public class BidRequest
    {
        [JsonProperty("bids")]
        public IEnumerable<BidReq> Bids { get; set; }
    }

    public class BidReq
    {
        [JsonProperty("UserId")]
        public string UserId { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("BidPrice")]
        public string BidPrice { get; set; }

        [JsonProperty("InitialPrice")]
        public string InitialPrice { get; set; }

        [JsonProperty("ProductName")]
        public string ProductName { get; set; }
    }
}
