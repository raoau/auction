﻿using System;

namespace Auction.Models
{
    public class Bid
    {
        public Guid Id { get; set; }
        public Guid AuctionProductId { get; set; }
        public Guid UserId { get; set; }
        public decimal BidPrice { get; set; }
        public DateTime BidTime { get; set; }
    }
}
