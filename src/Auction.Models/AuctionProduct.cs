﻿using System;

namespace Auction.Models
{
    public class AuctionProduct
    {
        public string Id { get; set; }
        public string AuctionId { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string InitialPrice { get; set; }
        public string SoldPrice { get; set; }
    }
}
