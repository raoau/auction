﻿using Auction.Entities.Context;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Bid, Models.Bid>();
            CreateMap<AuctionProduct, Models.AuctionProduct>();
        }
    }
}
