﻿using Auction.Entities.Context;
using Auction.Models;
using Auction.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BidController : ControllerBase
    {
        private readonly IBidService _bidService;

        public BidController(IBidService bidService)
        {
            _bidService = bidService;
        }

        // GET api/values
        [HttpGet]
        [Route("auctionproducts")]
        public async Task<IActionResult> Get()
        {
            var res = await _bidService.GetLatestAuctionProducts();
            return Ok(res);
        }

        // GET api/values
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var res = await _bidService.GetHighestBid(id);
            return Ok(res);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Bid([FromBody] BidRequest BidRequest)
        {
            foreach (var req in BidRequest.Bids)
            {
                await _bidService.BidProduct(new Guid(req.UserId), new Guid(req.Id), Convert.ToDecimal(req.BidPrice));
            }
            
            return Ok();
        }
    }
}
