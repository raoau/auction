﻿using Auction.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace BrandLink.API.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class GlobalExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configuration;

        public GlobalExceptionHandlerMiddleware(RequestDelegate next,
            IConfiguration configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (System.Exception ex)
            {
                await RespondError(context, ex);
            }
        }

        private async Task RespondError(HttpContext context, System.Exception ex)
        {
            string customMessage;
            if (ex is EAException eaEx)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                customMessage = eaEx.ToJson();
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                customMessage = "EdApp API Internal Exception.";
            }

            await context.Response.WriteAsync(customMessage);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class GlobalExceptionHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<GlobalExceptionHandlerMiddleware>();
        }
    }
}