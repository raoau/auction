﻿CREATE TABLE [dbo].[Bid] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [AuctionProductId] UNIQUEIDENTIFIER NOT NULL,
    [UserId]           UNIQUEIDENTIFIER NOT NULL,
    [BidPrice]         DECIMAL (18)     NOT NULL,
	[BidTime]		   DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Bid_AuctionProduct] FOREIGN KEY ([AuctionProductId]) REFERENCES [dbo].[AuctionProduct] ([Id]),
    CONSTRAINT [FK_Bid_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

