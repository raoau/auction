﻿CREATE TABLE [dbo].[AuctionProduct] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [AuctionId]    UNIQUEIDENTIFIER NOT NULL,
    [ProductId]    UNIQUEIDENTIFIER NOT NULL,
    [InitialPrice] DECIMAL (18)     NOT NULL,
    [SoldPrice]    DECIMAL (18)     NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AuctionProduct_Auction] FOREIGN KEY ([AuctionId]) REFERENCES [dbo].[Auction] ([Id]),
    CONSTRAINT [FK_AuctionProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id])
);

