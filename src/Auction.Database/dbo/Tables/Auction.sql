﻿CREATE TABLE [dbo].[Auction] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [Name]      NVARCHAR (100)   NOT NULL,
    [BeginTime] DATETIME         NOT NULL,
    [EndTime]   DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

