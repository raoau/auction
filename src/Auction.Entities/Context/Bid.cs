﻿using System;
using System.Collections.Generic;

namespace Auction.Entities.Context
{
    public partial class Bid
    {
        public Guid Id { get; set; }
        public Guid AuctionProductId { get; set; }
        public Guid UserId { get; set; }
        public decimal BidPrice { get; set; }
        public DateTime BidTime { get; set; }

        public virtual AuctionProduct AuctionProduct { get; set; }
        public virtual User User { get; set; }
    }
}
