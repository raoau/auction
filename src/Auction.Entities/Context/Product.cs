﻿using System;
using System.Collections.Generic;

namespace Auction.Entities.Context
{
    public partial class Product
    {
        public Product()
        {
            AuctionProduct = new HashSet<AuctionProduct>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AuctionProduct> AuctionProduct { get; set; }
    }
}
