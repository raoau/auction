﻿using System;
using System.Collections.Generic;

namespace Auction.Entities.Context
{
    public partial class Auction
    {
        public Auction()
        {
            AuctionProduct = new HashSet<AuctionProduct>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }

        public virtual ICollection<AuctionProduct> AuctionProduct { get; set; }
    }
}
