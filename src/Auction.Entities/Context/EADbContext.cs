﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Auction.Entities.Context
{
    public partial class EADbContext : DbContext
    {
        public EADbContext()
        {
        }

        public EADbContext(DbContextOptions<EADbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Auction> Auction { get; set; }
        public virtual DbSet<AuctionProduct> AuctionProduct { get; set; }
        public virtual DbSet<Bid> Bid { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Auction>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BeginTime).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<AuctionProduct>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.InitialPrice).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SoldPrice).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Auction)
                    .WithMany(p => p.AuctionProduct)
                    .HasForeignKey(d => d.AuctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AuctionProduct_Auction");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.AuctionProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AuctionProduct_Product");
            });

            modelBuilder.Entity<Bid>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BidPrice).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BidTime).HasColumnType("datetime");

                entity.HasOne(d => d.AuctionProduct)
                    .WithMany(p => p.Bid)
                    .HasForeignKey(d => d.AuctionProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Bid_AuctionProduct");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Bid)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Bid_User");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
