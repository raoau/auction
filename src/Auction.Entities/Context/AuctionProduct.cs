﻿using System;
using System.Collections.Generic;

namespace Auction.Entities.Context
{
    public partial class AuctionProduct
    {
        public AuctionProduct()
        {
            Bid = new HashSet<Bid>();
        }

        public Guid Id { get; set; }
        public Guid AuctionId { get; set; }
        public Guid ProductId { get; set; }
        public decimal InitialPrice { get; set; }
        public decimal? SoldPrice { get; set; }

        public virtual Auction Auction { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<Bid> Bid { get; set; }
    }
}
