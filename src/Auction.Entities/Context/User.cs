﻿using System;
using System.Collections.Generic;

namespace Auction.Entities.Context
{
    public partial class User
    {
        public User()
        {
            Bid = new HashSet<Bid>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Bid> Bid { get; set; }
    }
}
