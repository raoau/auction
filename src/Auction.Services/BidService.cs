﻿using Auction.Entities.Context;
using Auction.Exceptions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Services
{
    public class BidService : IBidService
    {
        private readonly EADbContext _context;
        private readonly IMapper _mapper;

        public BidService(EADbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<Models.AuctionProduct>> GetLatestAuctionProducts()
        {
            var auction = _context.Auction.OrderByDescending(x => x.BeginTime).FirstOrDefault();
            var auctionProducts = await _context.AuctionProduct.Where(x => x.AuctionId == auction.Id)
                .Select(x => new Models.AuctionProduct
                {
                     Id = x.Id.ToString(),
                     AuctionId = x.AuctionId.ToString(),
                     ProductId = x.ProductId.ToString(),
                     ProductName = x.Product.Name,
                     InitialPrice = $"{x.InitialPrice}",
                })
                .ToListAsync();

            return auctionProducts;
        }

        public async Task<Models.Bid> BidProduct(Guid userId, Guid auctionProductId, decimal bidPrice)
        {
            var bid = new Bid
            {
                Id = Guid.NewGuid(),
                AuctionProductId = auctionProductId,
                UserId = userId,
                BidPrice = bidPrice,
                BidTime = DateTime.UtcNow,
            };

            await _context.Bid.AddAsync(bid);

            await _context.SaveChangesAsync();

            return _mapper.Map<Models.Bid>(bid);
        }

        public async Task<Models.Bid> GetHighestBid(Guid auctionProductId)
        {
            var auctionProduct = _context.AuctionProduct.Include(x => x.Auction).FirstOrDefault(x => x.Id == auctionProductId);

            var dt = DateTime.UtcNow;
            if (dt < auctionProduct.Auction.EndTime)
            {
                throw new AuctionNotFinishedException()
                {
                    AuctionId = auctionProduct.AuctionId,
                    EndTime = auctionProduct.Auction.EndTime
                };
            }

            var bid = _context.Bid.Where(x => x.AuctionProductId == auctionProduct.Id)
                .OrderByDescending(x => x.BidPrice)
                .FirstOrDefault();

            auctionProduct.SoldPrice = bid.BidPrice;

            await _context.SaveChangesAsync();

            return _mapper.Map<Models.Bid>(bid);
        }
    }
}
