﻿using Auction.Entities.Context;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Services
{
    public interface IBidService
    {
        Task<List<Models.AuctionProduct>> GetLatestAuctionProducts();

        Task<Models.Bid> BidProduct(Guid userId, Guid auctionProductId, decimal bidPrice);

        Task<Models.Bid> GetHighestBid(Guid auctionProductId);
    }
}
