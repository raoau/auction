﻿using System;

namespace Auction.Exceptions
{
    [Flags]
    public enum EAExceptionEnum
    {
        Success = 0,

        AuctionNotFinishedYet = 1,

        // to be extended
    }
}
