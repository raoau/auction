﻿using Newtonsoft.Json;
using System;

namespace Auction.Exceptions
{
    public class AuctionNotFinishedException : EAException
    {
        public Guid AuctionId { get; set; }

        public DateTime EndTime { get; set; }

        public override string ToJson()
        {
            return JsonConvert.SerializeObject(new
            {
                StatusCode = EAExceptionEnum.AuctionNotFinishedYet,
                AuctionId,
                EndTime
            });
        }
    }
}
