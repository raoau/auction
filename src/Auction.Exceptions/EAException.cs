﻿using System;

namespace Auction.Exceptions
{
    public abstract class EAException : System.Exception
    {
        public int StatusCode { get; set; }

        public abstract string ToJson();    
    }
}
