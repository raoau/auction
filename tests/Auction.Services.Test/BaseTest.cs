using AutoMapper;
using Auction.Entities.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Auction.Services;

namespace Auction.Services.Test
{
    public abstract class BaseTest
    {
        protected readonly ServiceProvider _serviceProvider;

        public BaseTest()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();

            var services = new ServiceCollection();
            services.AddSingleton<IConfiguration>(configuration);
            services.AddDbContext<EADbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IBidService, BidService>();

            _serviceProvider = services.BuildServiceProvider();
        }
    }
}