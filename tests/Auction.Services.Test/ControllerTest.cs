using Auction.Api.Controllers;
using Auction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Auction.Services.Test
{
    [TestClass]
    public class ControllerTest : BaseTest
    {
        [TestMethod]
        public void BidProduct_WithBidPrice_ReturnSamePrice()
        {
            // arrange 
            var bidPrice = 1.11M;
            var userId = new Guid();
            var auctionProductId = new Guid();
            var service = new Mock<IBidService>();
            service.Setup(x => x.BidProduct(userId, auctionProductId, bidPrice)).Returns(Task.FromResult(new Models.Bid { BidPrice = bidPrice }));

            var controller = new BidController(service.Object);

            // act
            var bids = new BidRequest
            {
                Bids = new List<BidReq>
                {
                     new BidReq
                     {
                         UserId = userId.ToString(),
                         Id = auctionProductId.ToString(),
                         BidPrice = $"{bidPrice}",
                         InitialPrice = $"0.01",
                         ProductName = "Something"
                     }
                }
            };
            var actionResult = controller.Bid(bids).Result as OkResult;

            // assert
            Assert.AreNotEqual(actionResult, null);
        }
    }
}